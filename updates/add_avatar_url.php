<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddAvatarUrl extends Migration
{
    public function up()
    {
        Schema::table('jz_geoguessrreminder_players', function (Blueprint $table) {
            $table->string('avatar', 255)->nullable()->after('allow_announce');
        });
    }

    public function down()
    {
        Schema::table('jz_geoguessrreminder_players', function (Blueprint $table) {
            $table->dropColumn('avatar');
        });
    }
}
