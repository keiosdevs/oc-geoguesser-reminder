<?php namespace JZ\GeoguessrReminder\Models;

use Illuminate\Database\Eloquent\Collection;
use JZ\GeoguessrReminder\Classes\GGHelpers;
use Model;
use October\Rain\Database\Relations\BelongsToMany;

/**
 * Player Model
 * @property $mm_nick
 * @property $gg_nick
 * @property $leagues
 * @property $challenges
 * @method BelongsToMany leagues
 * @method BelongsToMany challenges
 * @property $allow_announce
 */
class Player extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jz_geoguessrreminder_players';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'leagues'    => [
            League::class,
            'table' => 'jz_geoguessrreminder_players_leagues',
            'pivot' => ['position', 'is_notified', 'points', 'score'],
            'pivotModel' => PlayerLeaguePivot::class
        ],
        'challenges' => [
            Challenge::class,
            'table' => 'jz_geoguessrreminder_players_legs',
            'pivot' => ['position', 'points'],
            'pivotModel' => PlayerChallengePivot::class
        ]
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getGoldLeagues(): Collection
    {
        return $this->leagues()->wherePivot('position', 1)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getSilverLeagues(): Collection
    {
        return $this->leagues()->wherePivot('position', 2)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getBronzeLeagues(): Collection
    {
        return $this->leagues()->wherePivot('position', 3)->get();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getGoldMedals(): Collection
    {
        return $this->challenges()->wherePivot('position', 1)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getSilverMedals(): Collection
    {
        return $this->challenges()->wherePivot('position', 2)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getBronzeMedals(): Collection
    {
        return $this->challenges()->wherePivot('position', 3)->get();
    }

    /**
     * @return array
     */
    public function getMedals(): array
    {
        $gold = $this->getGoldMedals()->count();
        $silver = $this->getSilverMedals()->count();
        $bronze = $this->getBronzeMedals()->count();
        $leagueBronze = $this->getBronzeLeagues()->count();
        $leagueSilver = $this->getSilverLeagues()->count();
        $leagueGold = $this->getGoldLeagues()->count();
        return [
            'gold'         => $gold,
            'gold_value'   => $gold * GGHelpers::GOLD_VALUE,
            'silver'       => $silver,
            'silver_value' => $silver * GGHelpers::SILVER_VALUE,
            'bronze'       => $bronze,
            'bronze_value' => $bronze * GGHelpers::BRONZE_VALUE,
            'total'        => $this->calculateTotalPoints(
                $bronze,
                $silver,
                $gold,
                $leagueBronze,
                $leagueSilver,
                $leagueGold
            ),

            'player' => $this
        ];
    }

    public function getNick(){
        if($this->mm_nick){
            return $this->mm_nick;
        }
        return $this->gg_nick;
    }

    private function calculateTotalPoints(
        int $bronze,
        int $silver,
        int $gold,
        int $leagueBronze,
        int $leagueSilver,
        int $leagueGold
    ) {
        return ($gold * GGHelpers::GOLD_VALUE)
               + ($silver * GGHelpers::SILVER_VALUE)
               + ($bronze * GGHelpers::BRONZE_VALUE)
               + ($leagueBronze * GGHelpers::BRONZE_LEAGUE_VALUE)
               + ($leagueSilver * GGHelpers::SILVER_LEAGUE_VALUE)
               + ($leagueGold * GGHelpers::GOLD_LEAGUE_VALUE);
    }

    public function scopeApplyLeagueFilter($query, $filtered)
    {
        return $query->whereHas('leagues', function($q) use ($filtered) {
            $q->whereIn('id', $filtered);
        });
    }

    public function scopeApplyChallengeFilter($query, $filtered)
    {
        return $query->whereHas('challenges', function($q) use ($filtered) {
            $q->whereIn('id', $filtered);
        });
    }

}
