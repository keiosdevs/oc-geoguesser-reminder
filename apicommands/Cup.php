<?php

namespace JZ\GeoguessrReminder\ApiCommands;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use Twig;

/**
 * Class Challenges
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Cup extends BaseCommand
{
    /**
     * @var string
     */
    protected $template = 'plugins/jz/geoguessrreminder/templates/cup.twig';

    /**
     * @var ChallengeRepository
     */
    private $challengeRepository;

    /**
     * @var PlayerRepository
     */
    private $playerRepository;

    /**
     * Challenges constructor.
     *
     * @param ChallengeRepository $repository
     * @param PlayerRepository    $playerRepository
     */
    public function __construct(ChallengeRepository $repository, PlayerRepository $playerRepository)
    {
        $this->challengeRepository = $repository;
        $this->playerRepository = $playerRepository;
    }

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        $players = $this->playerRepository->getMattermostPlayers();
        $list = [];
        foreach ($players as $player) {
            $list[$player->mm_nick] = $player->getMedals();
            $list = array_reverse(array_sort($list, static function ($el) {
                return $el['total'];
            }));
        }

        $params = [
            'cup_list' => $list
        ];

        return GGApiResponse::send($this->getContent($params), $payload->type);
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return 'shows ' . Carbon::now()->year . ' Cup table';
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function getContent(array $params): string
    {
        $template = file_get_contents(base_path($this->template));
        return Twig::parse($template, $params);
    }
}
