<?php


namespace JZ\GeoguessrReminder\ValueObjects;

use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\Models\Settings;

/**
 * Class GeoGuessrChallenge
 * @package JZ\GeoguessrReminder\ValueObjects
 */
class GeoGuessrChallenge
{
    /**
     * @var mixed
     */
    public $mapName;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $hash;

    /**
     * @var array
     */
    public $positions = [];

    /**
     * @var GeoGuessrMap|null
     */
    public $map;

    public $league;

    /**
     * GeoGuessrChallenge constructor.
     *
     * @param array $data
     */
    public function __construct(array $data, League $league = null, GeoGuessrMap $map = null)
    {
        $this->map = $map;
        /** @var Settings $settings */
        $settings = Settings::instance();
        $nickMap = $settings->getNickMap();
        if ($league) {
            $this->league = $league;
        }
        if (isset($data[0]) && isset($data[0]['game'])) {
            $this->mapName = $data[0]['game']['mapName'];
        }
        if (isset($data[0]['hash'])) {
            $this->hash = $data[0]['hash'];
            $this->url = 'https://www.geoguessr.com/challenge/' . $this->hash;
        }
        $result = [];
        foreach ($data as $pos => $details) {
            if (!is_array($details)) {
                continue;
            }
            $nick = $details['playerName'];
            if (array_key_exists($nick, $nickMap)) {
                $nick = $nickMap[$nick];
            }
             $result[$pos] = new Position(
                 $pos,
                 $nick,
                 0,
                 $details['totalScore'],
                 $details['pinUrl']
             );
        }
        $this->positions = $result;
    }

    /**
     * @param $seconds
     *
     * @return string
     */
    public function time($seconds)
    {
        $t = round($seconds);
        return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
    }
}
