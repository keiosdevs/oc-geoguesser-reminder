<?php


namespace JZ\GeoguessrReminder\ApiCommands;

use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use Twig;

/**
 * Class Welcome
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Welcome extends BaseCommand
{
    public const TEMPLATE = 'plugins/jz/geoguessrreminder/templates/welcome.twig';

    /**
     * @var ChallengeRepository
     */
    private $challengeRepository;

    /**
     * @var LeagueRepository
     */
    private $leagueRepository;

    /**
     * ChallengeResults constructor.
     *
     * @param ChallengeRepository $challengeRepository
     */
    public function __construct(ChallengeRepository $challengeRepository, LeagueRepository $leagueRepository)
    {
        $this->challengeRepository = $challengeRepository;
        $this->leagueRepository = $leagueRepository;
    }

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        $player = $payload->player;
        return GGApiResponse::send($this->generateMenu($player), $payload->type);
    }

    /**
     * @param string $user
     *
     * @return string
     */
    private function generateMenu(string $user): string
    {
        return $this->getContent(['user' => $user,
                                  'leagues' => $this->leagueRepository->getEnabled(),
                                  'challenges' => $this->challengeRepository->getActive()]);
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return '';
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function getContent(array $params): string
    {
        return Twig::parse(
            file_get_contents(base_path(self::TEMPLATE)),
            $params
        );
    }
}
