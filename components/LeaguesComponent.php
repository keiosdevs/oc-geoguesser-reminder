<?php namespace JZ\GeoguessrReminder\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\Classes\RequestSender;

/**
 * Class LeaguesComponent
 * @package JZ\GeoguessrReminder\Components
 */
class LeaguesComponent extends ComponentBase
{
    /**
     * @var LeagueRepository|mixed
     */
    private $leagueRepository;
    /**
     * @var ChallengeRepository|mixed
     */
    private $challengeRepository;

    /**
     * LeaguesComponent constructor.
     *
     * @param CodeBase|null $cmsObject
     * @param array         $properties
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(CodeBase $cmsObject = null, $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->leagueRepository = app()->make(LeagueRepository::class);
        $this->challengeRepository = app()->make(ChallengeRepository::class);
    }


    /**
     * @return string[]
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'Leagues Component',
            'description' => 'Displays challenges'
        ];
    }

    /**
     * @return array
     */
    public function defineProperties(): array
    {
        return [
            'history_mode' => [
                'title'       => 'History Mode',
                'description' => 'Enable History Mode',
                'default'     => false,
                'type'        => 'switch',
            ],
            'code' => [
                'title' => 'Code',
                'description' => 'League code',
                'default' => '{{ :code }}',
                 'type' => 'string'

            ]
        ];
    }

    /**
     *
     */
    public function onRun(): void
    {
        if($this->property('history_mode')){
            $this->page['history_mode'] = true;
            $this->page['leagues'] = $l = $this->leagueRepository->getAllPaginated();
        } else {
            $this->page['leagues'] = $l = $this->leagueRepository->getEnabled();
        }
        if($code = $this->property('code')) {
            $this->page['selected_league'] = $this->leagueRepository->getBySlug($code);
        }
    }

    /**
     * @param League $league
     *
     * @return array|mixed
     */
    public function getList(League $league): array
    {
        return $league->getRemoteScoreList();
    }
}
