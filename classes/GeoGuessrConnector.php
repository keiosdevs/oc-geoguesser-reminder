<?php


namespace JZ\GeoguessrReminder\Classes;

use Cache;
use JZ\GeoguessrReminder\Exceptions\InvalidGeoGuesserResponseException;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\Models\Settings;
use Log;
use October\Rain\Exception\ApplicationException;

/**
 * Class GeoGuessrConnector
 * @package JZ\GeoguessrReminder\Classes
 */
class GeoGuessrConnector
{
    private const CACHE_TIME = 1;
    /**
     *
     */
    protected const LEAGUE_SCORES_ENDPOINT = 'https://www.geoguessr.com/api/v3/leagues/<SLUG>/scores/<PAGE>';
    /**
     *
     */
    protected const LEAGUE_DETAILS_ENDPOINT = 'https://www.geoguessr.com/api/v3/leagues/<SLUG>';
    /**
     *
     */
    protected const CHALLENGE_SCORES_ENDPOINT = 'https://www.geoguessr.com/api/v3/results/scores/<SLUG>/0/26';
    /**
     *
     */
    protected const CHALLENGE_DETAILS_ENDPOINT = 'https://www.geoguessr.com/api/v3/challenges/<SLUG>';

    /**
     * @return string
     * @throws ApplicationException
     */
    public static function getToken(): string
    {
        $token = Settings::instance()->get('gg_token', env('GG_TOKEN', ''));
        if (!$token) {
            throw new ApplicationException('No Geoguesser token provided!');
        }
        return $token;
    }

    /**
     * @param League $league
     *
     * @return array
     * @throws InvalidGeoGuesserResponseException
     */
    public function getRemoteScoreList(League $league): array
    {
        $requestSender = new RequestSender();
        $cacheKey = 'aleague_' . $league->slug;
        $page = 0;
        $url = str_replace(['<SLUG>','<PAGE>'], [$league->slug, $page], self::LEAGUE_SCORES_ENDPOINT);
        $responsesList = [];
        $response = $this->makeCachedRequest($requestSender, $cacheKey, $url);
        $responsesList[] = $response;
        while (array_key_exists('hasMoreScores', $response) && $response['hasMoreScores'] === true && $page < 50) {
            ++$page;
            $cacheKey .= $page;
            $url = str_replace(['<SLUG>','<PAGE>'], [$league->slug, $page], self::LEAGUE_SCORES_ENDPOINT);

            $response = $this->makeCachedRequest($requestSender, $cacheKey, $url);
            $responsesList[] = $response;
        }
        $scores = [];
        $count = 0;
        foreach ($responsesList as $response) {
            foreach ($response['scores'] as $scoresEntry) {
                $scores[$count] = $scoresEntry;
                ++$count;
            }
        }

        return $scores;
    }

    /**
     * @param League $league
     *
     * @return array
     * @throws InvalidGeoGuesserResponseException
     */
    public function getRemoteDetails(League $league): array
    {
        $requestSender = new RequestSender();
        $cacheKey = 'fleague_' . $league->slug;
        $url = str_replace('<SLUG>', $league->slug, self::LEAGUE_DETAILS_ENDPOINT);
        return $this->makeCachedRequest($requestSender, $cacheKey, $url);
    }

    /**
     * @param string $challengeId
     *
     * @return array
     * @throws InvalidGeoGuesserResponseException
     */
    public function getChallengeDetails(string $challengeId): array
    {
        $requestSender = new RequestSender();
        $cacheKey = 'dasvvsdaa' . $challengeId;
        $url = str_replace('<SLUG>', $challengeId, self::CHALLENGE_DETAILS_ENDPOINT);
        return $this->makeCachedRequest($requestSender, $cacheKey, $url);
    }

    /**
     * @param string $challengeId
     *
     * @return array
     * @throws InvalidGeoGuesserResponseException
     */
    public function getChallengeScores(string $challengeId): array
    {
        $requestSender = new RequestSender();
        $cacheKey = 'ccccaaaa' . $challengeId;
        $url = str_replace('<SLUG>', $challengeId, self::CHALLENGE_SCORES_ENDPOINT);
        return $this->makeCachedRequest($requestSender, $cacheKey, $url);
    }


    /**
     * @param RequestSender $requestSender
     * @param string        $cacheKey
     * @param string        $url
     *
     * @return array
     * @throws InvalidGeoGuesserResponseException
     */
    private function makeCachedRequest(RequestSender $requestSender, string $cacheKey, string $url): array
    {
        if (!\Cache::has($cacheKey)) {
            \Log::info('Uncached request to GeoGuessr! '. $url);
        }
        $results = Cache::remember($cacheKey, self::CACHE_TIME, function () use ($requestSender, $url) {
            $requestSender->addHeader('Cookie: _ncfa='. self::getToken() . ';');

            return $requestSender->sendGetRequest(['limit' => 10], $url);
        });

        if (array_key_exists('content', $results)) {
            $response = json_decode($results['content'], true);
            $this->validateResponse($response, $cacheKey);

            if ($response) {
                return $response;
            }
        }
        return [];
    }

    /**
     * @param $response
     * @param $cacheKey
     *
     * @throws InvalidGeoGuesserResponseException
     */
    private function validateResponse($response, $cacheKey): void
    {
        if (is_string($response)) {
            Log::error('Error in Scores api request: ' . print_r($response, true));
            \Cache::forget($cacheKey);
            throw new InvalidGeoGuesserResponseException($response);
        }
        if (isset($response['message'])) {
            Log::error('Error in Scores api request: ' . print_r($response['message'], true));
            \Cache::forget($cacheKey);
            throw new InvalidGeoGuesserResponseException($response['message']);
        }
    }
}
