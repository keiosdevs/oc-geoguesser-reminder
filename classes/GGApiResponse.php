<?php


namespace JZ\GeoguessrReminder\Classes;

use Illuminate\Http\JsonResponse;

class GGApiResponse
{
    public static function send(string $text, string $type = 'ephemeral'): JsonResponse
    {
        if ($type === 'ephemeral') {
            $type = env('MM_TYPE', 'ephemeral');
        }
        return response()->json(['response_type' => $type, 'text' => $text], 200);
    }
}
