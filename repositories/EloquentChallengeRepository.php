<?php


namespace JZ\GeoguessrReminder\Repositories;

use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use JZ\GeoguessrReminder\Classes\GGHelpers;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\Models\Challenge;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use JZ\GeoguessrReminder\ValueObjects\Position;
use October\Rain\Database\Collection;

class EloquentChallengeRepository implements ChallengeRepository
{
    public const cacheKey = 'challe';

    private $playerRepository;
    private $leagueRepository;

    public function __construct(PlayerRepository $playerRepository, LeagueRepository $leagueRepository)
    {
        $this->playerRepository = $playerRepository;
        $this->leagueRepository = $leagueRepository;
    }

    public function getById(int $id): ?Challenge
    {
        return Challenge::where('id', $id)
                        ->with('players')
                        ->with('league')
                        ->rememberForever(self::cacheKey . $id)
                        ->first();
    }

    public function getByHash(string $hash): ?Challenge
    {
        return Challenge::where('hash', $hash)
                        ->with('players')
                        ->with('league')
                        ->rememberForever(self::cacheKey . $hash)
                        ->first();
    }

    public function getActive(): Collection
    {
        return Challenge::where('is_enabled', true)
                        ->orderBy('created_at', 'desc')
                        ->with('players')
                        ->with('league')
                        ->rememberForever(self::cacheKey . '_active')
                        ->get();
    }

    public function synchronize(GeoGuessrChallenge $challenge, League $league = null): Challenge
    {
        $baseDate = null;
        $challengeModel = $this->getByHash($challenge->map->code);
        if (!$challengeModel) {
            $challengeModel = new Challenge();
            $baseDate = Carbon::now();
            $challengeModel->is_enabled = true;
            if ($league) {
                $challengeModel->ends_at = $league->getChallengeEndTime();
            }
        }
        if ($challengeModel->league) {
            $league = $challengeModel->league;
        }
        if (!$baseDate) {
            $baseDate = new Carbon($challengeModel->created_at);
        }
        if (!$league && !$challengeModel->ends_at) {
            $challengeModel->ends_at = $baseDate->addWeek();
        }
        $challengeModel->hash = $challenge->map->code;
        $challengeModel->mapName = $challenge->map->name;
        $challengeModel->url = $challenge->map->url;
        $challengeModel->positions = json_encode($challenge->positions);
        $challengeModel->noMove = $challenge->map->noMove;
        $challengeModel->noZoom = $challenge->map->noZoom;
        $challengeModel->noRotate = $challenge->map->noRotate;
        $challengeModel->time = $challenge->map->time;
        $challengeModel->rounds = $challenge->map->rounds;

        if ($league) {
            $challengeModel->league_id = $league->id;
        }

        $endsAt = new Carbon($challengeModel->ends_at);
        $now = Carbon::now();
        if (!$challengeModel->league_id && $endsAt > $now) {
            $challengeModel->is_enabled = true;
        }
        if ($endsAt < $now) {
            $challengeModel->is_enabled = false;
        }
        $challengeModel->save();

        if ($challenge->positions) {
            /** @var Position $position */
            foreach ($challenge->positions as $position) {
                $baseNick = $position->player;
                if (str_contains($baseNick, '@')) {
                    $ggNick = GGHelpers::findGeoguessrNickOnMap($baseNick);
                    $nickList[$ggNick] = $baseNick;
                } else {
                    $nickList[$baseNick] = GGHelpers::findMmNickOnMap($baseNick);
                    $ggNick = $baseNick;
                }
                $player = $this->playerRepository->getByGeoguessrName($ggNick);
                if (!$player) {
                    $player = $this->playerRepository->createPlayer($ggNick);
                }
                if ($challengeModel->league_id && !$player->leagues->where('id', $challengeModel->league_id)->first()) {
                    $player->leagues()->add($challengeModel->league);
                }
                if ($player->challenges->where('id', $challengeModel->id)->first()) {
                    $player->challenges()->remove($challengeModel);
                    $player->save();
                }
                $pos = $position->pos + 1;
                $player->challenges()->add(
                    $challengeModel,
                    null,
                    [
                        'points'   => $position->score,
                        'position' => $pos
                    ]
                );
                $player->save();
            }
        }

        return $challengeModel;
    }

    public function getAll(): Collection
    {
        return Challenge::all();
    }

    public function getAllPaginated($perPage = 10): LengthAwarePaginator
    {
        return Challenge::orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function deactivateOldLegs(League $league, string $currentLegHash)
    {
        if (!$league) {
            return;
        }

        Challenge::where('league_id', $league->id)->where('hash', '!=', $currentLegHash)->where('is_enabled', true)
                                                                                        ->update([
                                                                                            'is_enabled' => false
                                                                                                 ]);
    }
}
