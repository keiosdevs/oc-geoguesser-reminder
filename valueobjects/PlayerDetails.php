<?php


namespace JZ\GeoguessrReminder\ValueObjects;


use JZ\GeoguessrReminder\Models\Challenge;

class PlayerDetails
{
    private $mmNick;
    private $ggNick;
    private $leagues;
    private $challenges;

    public function __construct(Challenge $challenge)
    {

    }


    /**
     * @return mixed
     */
    public function getMmNick()
    {
        return $this->mmNick;
    }

    /**
     * @return mixed
     */
    public function getGgNick()
    {
        return $this->ggNick;
    }

    /**
     * @return mixed
     */
    public function getLeagues()
    {
        return $this->leagues;
    }

    /**
     * @return mixed
     */
    public function getChallenges()
    {
        return $this->challenges;
    }
}
