<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLeaguesTable extends Migration
{
    public function up()
    {
        Schema::create('jz_geoguesserreminder_leagues', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->mediumText('players')->nullable();
            $table->dateTime('end_date');
            $table->boolean('is_enabled')->default(false);
            $table->integer('rounds_left')->default(10);
            $table->integer('round_time')->default(48);
            $table->string('hours_to_remind')->default('8,4,2');
            $table->string('flags')->nullable();
            $table->string('url')->nullable();
            $table->string('blacklist_users')->nullable();
            $table->string('custom_hook')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_geoguesserreminder_leagues');
    }
}
