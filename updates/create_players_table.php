<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePlayersTable extends Migration
{
    public function up()
    {
        Schema::create('jz_geoguessrreminder_players', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('mm_nick')->index()->nullable();
            $table->string('gg_nick')->index()->nullable();
            $table->boolean('allow_announce')->default(true);
            $table->timestamps();
        });
        Schema::create('jz_geoguessrreminder_players_leagues', function (Blueprint $table) {
            $table->integer('player_id')->index();
            $table->integer('league_id')->index();
            $table->integer('position')->nullable();
            $table->boolean('is_notified')->default(false);
        });
        Schema::create('jz_geoguessrreminder_players_legs', function (Blueprint $table) {
            $table->integer('player_id')->index();
            $table->string('challenge_id')->index();
            $table->integer('position')->nullable();
            $table->integer('points')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_geoguessrreminder_players');
        Schema::dropIfExists('jz_geoguessrreminder_players_leagues');
        Schema::dropIfExists('jz_geoguessrreminder_players_legs');
    }
}
