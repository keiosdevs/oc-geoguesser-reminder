<?php


namespace JZ\GeoguessrReminder\ApiCommands;

use Hashids\Hashids;
use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GeoGuessrConnector;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\Exceptions\InvalidGeoGuesserResponseException;
use JZ\GeoguessrReminder\Models\Player;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrMap;
use Twig;

/**
 * Class Announce
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Token extends BaseCommand
{
    public const TEMPLATE = 'plugins/jz/geoguessrreminder/templates/token.twig';

    private $playerRepository;

    public function __construct(PlayerRepository $playerRepository){
        $this->playerRepository = $playerRepository;
    }
    /**
     * @param CommandPayload    $payload
     *
     * @return JsonResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @var ChallengeRepository $repo
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        $player = $this->playerRepository->getByMattermostName($payload->player);
        if($player){
            $token = $this->generateToken($player);
            $player->token = $token;
            $player->save();
            return GGApiResponse::send('Your token is: `'.$token.'`');
        }
        return GGApiResponse::send('You are not GeoGuesser player. Please submit yourself
         with /gg submit or contact @jakub', 'ephemeral');
    }

    private function generateToken(Player  $player){
        return 'gg'.bin2hex(random_bytes(16));
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return 'announces GeoGuessr challenge (do not use for leagues)';
    }

    /**
     * @param array $variables
     *
     * @return string
     */
    public function getContent(array $variables): string
    {
        $template = file_get_contents(base_path(self::TEMPLATE));
        return Twig::parse($template, $variables);
    }
}
