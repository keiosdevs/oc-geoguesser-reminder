<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddDescriptionToTables extends Migration
{
    public function up()
    {
        Schema::table('jz_geoguesserreminder_challenges', function (Blueprint $table) {
            $table->text('description')->nullable()->after('hash');
        });
        Schema::table('jz_geoguesserreminder_leagues', function (Blueprint $table) {
            $table->text('description')->nullable()->after('name');
        });
    }

    public function down()
    {
        Schema::table('jz_geoguesserreminder_challenges', function (Blueprint $table) {
            $table->dropColumn('description');
        });
        Schema::table('jz_geoguesserreminder_leagues', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
