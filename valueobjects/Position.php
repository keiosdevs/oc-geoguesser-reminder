<?php


namespace JZ\GeoguessrReminder\ValueObjects;


class Position
{
    public $pos;
    public $player;
    public $points;
    public $score;
    public $image;

    public function __construct(int $pos, string $name, int $points, int $score, string $image = null)
    {
        $this->pos = $pos;
        $this->player = $name;
        $this->points = $points;
        $this->score = $score;
        $this->image = $image;
    }
}
