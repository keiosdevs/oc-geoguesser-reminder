<?php

use JZ\GeoguessrReminder\Controllers\ApiController;

Route::any(
    'v1/gg',
    static function (Request $request) {
        $controller = new ApiController();
        return $controller->manage();
    }
);
