<?php


namespace JZ\GeoguessrReminder\Contracts;


use Illuminate\Pagination\LengthAwarePaginator;
use JZ\GeoguessrReminder\Models\League;
use October\Rain\Database\Collection;

/**
 * Interface LeagueRepository
 * @package JZ\GeoguessrReminder\Contracts
 */
interface LeagueRepository
{
    /**
     * @param int $leagueId
     *
     * @return League|null
     */
    public function getById(int $leagueId): ?League;

    /**
     * @param string $slug
     *
     * @return League|null
     */
    public function getBySlug(string $slug): ?League;

    /**
     * @return Collection|League[]
     */
    public function getEnabled(): Collection;

    /**
     * @return Collection|League[]
     */
    public function getAll(): Collection;

    /**
     * @return LengthAwarePaginator
     */
    public function getAllPaginated(int $perPage = 10): LengthAwarePaginator;

    /**
     * @param League $league
     * @param array  $scoreList
     *
     * @return League
     */
    public function synchronize(League $league, array $scoreList): League;
}
