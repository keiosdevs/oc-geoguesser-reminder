<?php namespace JZ\GeoguessrReminder;

use Backend;
use JZ\GeoguessrReminder\ApiCommands\Announce;
use JZ\GeoguessrReminder\ApiCommands\ChallengeResults;
use JZ\GeoguessrReminder\ApiCommands\Challenges;
use JZ\GeoguessrReminder\ApiCommands\Cup;
use JZ\GeoguessrReminder\ApiCommands\Help;
use JZ\GeoguessrReminder\ApiCommands\Info;
use JZ\GeoguessrReminder\ApiCommands\LeagueResults;
use JZ\GeoguessrReminder\ApiCommands\Me;
use JZ\GeoguessrReminder\ApiCommands\RestoreNotifications;
use JZ\GeoguessrReminder\ApiCommands\Resync;
use JZ\GeoguessrReminder\ApiCommands\StopNotifications;
use JZ\GeoguessrReminder\ApiCommands\Time;
use JZ\GeoguessrReminder\ApiCommands\Token;
use JZ\GeoguessrReminder\Components\ChallengesComponent;
use JZ\GeoguessrReminder\Components\LeaguesComponent;
use JZ\GeoguessrReminder\Components\PlayerComponent;
use JZ\GeoguessrReminder\Console\GGCommand;
use JZ\GeoguessrReminder\Console\Remind;
use JZ\GeoguessrReminder\Console\ScanPlayers;
use JZ\GeoguessrReminder\Console\Sync;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\Models\Settings;
use JZ\GeoguessrReminder\Repositories\EloquentChallengeRepository;
use JZ\GeoguessrReminder\Repositories\EloquentLeagueRepository;
use JZ\GeoguessrReminder\Repositories\EloquentPlayerRepository;
use System\Classes\PluginBase;

/**
 * GeoguessrReminder Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'GeoguessrReminder',
            'description' => 'No description provided yet...',
            'author'      => 'JZ',
            'icon'        => 'icon-globe'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register(): void
    {
        $this->commands([
                            Remind::class,
                            Sync::class,
                            GGCommand::class,
                            ScanPlayers::class
                        ]);
        $this->app->bind(LeagueRepository::class, EloquentLeagueRepository::class);
        $this->app->bind(PlayerRepository::class, EloquentPlayerRepository::class);
        $this->app->bind(ChallengeRepository::class, EloquentChallengeRepository::class);

        // todo - methods below should be provided by a dedicated ServiceProvider
        $this->registerApiCommands();
    }

    public function registerApiCommands(): void
    {

        $this->app['api_commands'] = [
            'help'       => [
                'class' => Help::class
            ],
            'me'         => [
                'class' => Me::class,
            ],
            'info'       => [
                'class' => Info::class
            ],
            'challenges' => [
                'class' => Challenges::class,
            ],
            'cup'        => [
                'class' => Cup::class
            ],
            'time'       => [
                'class' => Time::class,
                'param' => 'LEAGUE_ID'
            ],
            'stop'       => [
                'class' => StopNotifications::class,
                'param' => 'LEAGUE_ID'
            ],
            'reset'      => [
                'class' => RestoreNotifications::class,
                'param' => 'LEAGUE_ID'
            ],
            'results'    => [
                'class' => LeagueResults::class,
                'param' => 'LEAGUE_ID'
            ],
            'legResults' => [
                'class' => ChallengeResults::class,
                'param' => 'CHALLENGE_ID/CHALLENGE_HASH/URL'
            ],
            'announce'   => [
                'class' => Announce::class,
                'param' => 'CHALLENGE_HASH/URL'
            ],
            'token'      => [
                'class' => Token::class
            ],
            'resync' => [
                'class' => Resync::class
            ]
        ];
    }
/*
    public function registerParamlessCommands(): void
    {
        $this->app['paramless_commands'] = [
            'challenges',
            'me',
            'help',
            'cup',
            'token',
            'resync'
        ];
    }

    public function registerParamlessLeagueCommands(): void
    {
        $this->app['paramless_league_commands'] = [
            'time',
            'reset',
            'stop',
            'results',
            'resync'
        ];
    }

    public function registerParamlessChallengeCommands(): void
    {
        $this->app['paramless_challenge_commands'] = [
            'legResults'
        ];
    }
*/
    public function registerSchedule($schedule): void
    {
        if(env('GEOG_ENABLED')) {
            $schedule->command('gg:remind')->everyMinute();
            $schedule->command('gg:sync')->everyFifteenMinutes();
        }
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot(): void
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents(): array
    {
        return [
            LeaguesComponent::class    => 'gg_leagues',
            ChallengesComponent::class => 'gg_challenges',
            PlayerComponent::class     => 'gg_player'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions(): array
    {
        return [
            'jz.geoguessrreminder.leagues'         => [
                'tab'   => 'GeoguessrReminder',
                'label' => 'Leagues Access'
            ],
            'jz.geoguessrreminder.challenges'      => [
                'tab'   => 'GeoguessrReminder',
                'label' => 'Challenges Access'
            ],
            'jz.geoguessrreminder.players'         => [
                'tab'   => 'GeoguessrReminder',
                'label' => 'Players Access'
            ],
            'jz.geoguessrreminder.access_settings' => [
                'tab'   => 'GeoguessrReminder',
                'label' => 'Settings Access'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation(): array
    {
        return [
            'geoguessrreminder' => [
                'label'       => 'GeoguessrReminder',
                'url'         => Backend::url('jz/geoguessrreminder/leagues'),
                'icon'        => 'icon-globe',
                'permissions' => ['jz.geoguessrreminder.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'leagues'    => [
                        'label'       => 'Leagues',
                        'url'         => Backend::url('jz/geoguessrreminder/leagues'),
                        'icon'        => 'icon-trophy',
                        'permissions' => ['jz.geoguessrreminder.leagues'],
                        'order'       => 100,
                    ],
                    'challenges' => [
                        'label'       => 'Challenges',
                        'url'         => Backend::url('jz/geoguessrreminder/challenges'),
                        'icon'        => 'icon-crosshairs',
                        'permissions' => ['jz.geoguessrreminder.challenges'],
                        'order'       => 200,
                    ],
                    'players'    => [
                        'label'       => 'Players',
                        'url'         => Backend::url('jz/geoguessrreminder/players'),
                        'icon'        => 'icon-users',
                        'permissions' => ['jz.geoguessrreminder.players'],
                        'order'       => 300,
                    ]
                ]
            ],
        ];
    }


    /**
     * @return array
     */
    public function registerSettings(): array
    {
        return [
            'messaging' => [
                'label'       => 'GeoGuessr Settings',
                'description' => 'Seetings for GeoGuessr Reminder',
                'category'    => 'Misc',
                'icon'        => 'icon-globe',
                'class'       => Settings::class,
                'permissions' => ['jz.geoguessrreminder.access_settings'],
                'order'       => 500,
                'keywords'    => 'geo nick',
            ],
        ];
    }

}
