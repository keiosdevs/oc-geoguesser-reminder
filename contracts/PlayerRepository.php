<?php


namespace JZ\GeoguessrReminder\Contracts;


use JZ\GeoguessrReminder\Models\Player;
use October\Rain\Database\Collection;

/**
 * Interface PlayerRepository
 * @package JZ\GeoguessrReminder\Contracts
 */
interface PlayerRepository
{
    /**
     * @param string $name
     *
     * @return Player|null
     */
    public function getByGeoguessrName(string $name): ?Player;

    /**
     * @return Player[]|Collection
     */
    public function getMattermostPlayers(): Collection;

    /**
     * @param string $ggNick
     *
     * @return Player
     */
    public function createPlayer(string $ggNick): Player;

    /**
     * @param string $player
     *
     * @return Player|null
     */
    public function getByMattermostName(string $player): ?Player;


    /**
     * @param string $token
     *
     * @return Player|null
     */
    public function getByToken(string $token): ?Player;
}
