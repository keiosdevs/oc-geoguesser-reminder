<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateChallengesTable extends Migration
{
    public function up()
    {
        Schema::create('jz_geoguesserreminder_challenges', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('mapName');
            $table->boolean('noMove')->default(false);
            $table->boolean('noZoom')->default(false);
            $table->boolean('noRotate')->default(false);
            $table->integer('time');
            $table->integer('rounds')->default(5);
            $table->string('hash');
            $table->string('url');
            $table->text('positions')->nullable();
            $table->dateTime('ends_at')->nullable();
            $table->integer('league_id')->nullable();
            $table->boolean('is_enabled')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jz_geoguesserreminder_challenges');
    }
}
