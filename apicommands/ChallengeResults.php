<?php


namespace JZ\GeoguessrReminder\ApiCommands;

use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GeoGuessrConnector;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Exceptions\InvalidGeoGuesserResponseException;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrMap;

/**
 * Class ChallengeResults
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class ChallengeResults extends BaseCommand
{
    protected $template = 'plugins/jz/geoguessrreminder/templates/challenge_results.twig';

    /**
     * @var ChallengeRepository
     */
    private $challengeRepository;

    /**
     * ChallengeResults constructor.
     *
     * @param ChallengeRepository $challengeRepository
     */
    public function __construct(ChallengeRepository $challengeRepository)
    {
        $this->challengeRepository = $challengeRepository;
    }

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        return GGApiResponse::send($this->generateTable($payload), $payload->type);
    }

    /**
     * @param CommandPayload $payload
     *
     * @return string
     */
    private function generateTable(CommandPayload $payload): string
    {
        if(is_numeric($payload->identifier)){
            $challenge = $this->challengeRepository->getById($payload->identifier);
        } else {
            $challenge = $this->challengeRepository->getByHash($payload->identifier);
        }
        if(!$challenge){
            return GGApiResponse::send('Challenge not found.', 'ephemeral');
        }
        $challengeHash = $challenge->hash;
        if ($payload->league) {
            $challengeHash = $payload->league->getCurrentLegId();
        }
        if (!$challengeHash) {
            return '';
        }
        try {
            $challengeDetails = (new GeoGuessrConnector())->getChallengeScores($challengeHash);
        } catch (InvalidGeoGuesserResponseException $e) {
            return 'Error: ' . $e->getMessage() . " (probably jakub didn't play yet)";
        }
        $challengeDetails[0]['hash'] = $challengeHash;
        $payload = $this->generateSilentAnnouncePayload($challengeHash);

        $map = new GeoGuessrMap($challengeDetails, $payload);
        $challengeObject = new GeoGuessrChallenge($challengeDetails, $payload->league, $map);
        $this->challengeRepository->synchronize($challengeObject);
        $variables = [
            'challenge' => $challengeObject,
            'league'    => $payload->league
        ];
        return $this->getContent($variables);
    }


    /**
     * @return string
     */
    public function getHelp(): string
    {
        return 'shows challenge results (if jakub played it!).';
    }
}
