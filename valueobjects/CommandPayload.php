<?php


namespace JZ\GeoguessrReminder\ValueObjects;

use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Controllers\ApiController;
use JZ\GeoguessrReminder\Models\Challenge;
use JZ\GeoguessrReminder\Models\League;

/**
 * Class CommandPayload
 * @package JZ\GeoguessrReminder\ValueObjects
 */
class CommandPayload
{
    /**
     * May be integer for old commands
     * @var string|null
     */
    public $command;

    /**
     * @var League|null
     */
    public $league;
    /**
     * @var string|null
     */
    public $player;
    /**
     * @var string|null
     */
    public $param1;
    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $type = 'ephemeral';
    /**
     * @var Challenge|null
     */
    public $challenge;

    /**
     * CommandPayload constructor.
     * todo - too much logic here, should be a factory
     *
     * @param array       $data
     * @param League|null $league
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(array $data)
    {
        $this->player = '@' . $data['user_name'];
        $data = explode(' ', $data['text']);

        $this->command = $data[0] ?? null;
        $this->identifier = $data[1] ?? null;
        $this->param1 = $data[2] ?? null;

        if (!is_numeric($this->identifier)) {
            preg_match("#(?!\/)\w{16}#", $this->identifier, $matches);
            if (count($matches) > 0) {
                $this->identifier = $matches[0];
            }
        }

        $this->setupType();
    }

    /**
     *
     */
    private function setupType(): void
    {
        if ($this->command === 'yell') {
            $this->command = null;
            $this->param1 = 'yell';
        }

        if ($this->identifier === 'yell') {
            $this->identifier = null;
            $this->param1 = 'yell';
        }

        if ($this->param1 === 'yell') {
            $this->type = 'in_channel';
        }

        if ($this->param1 === 'silent') {
            $this->type = 'ephemeral';
        }
    }
}
