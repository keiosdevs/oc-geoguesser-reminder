<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class DropDepreciatedFields extends Migration
{
    public function up()
    {
        Schema::table('jz_geoguesserreminder_leagues', function (Blueprint $table) {
            $table->dropColumn('players');
        });
    }

    public function down()
    {

    }
}
