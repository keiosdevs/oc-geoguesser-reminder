<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddNotifLeaguePivotFields extends Migration
{
    public function up()
    {
        Schema::table('jz_geoguessrreminder_players_leagues', function (Blueprint $table) {
            $table->boolean('is_notified')->nullable()->after('score')->default(true);
        });

    }

    public function down()
    {
        Schema::table('jz_geoguessrreminder_players_leagues', function (Blueprint $table) {
            $table->dropColumn('is_notified');
        });
    }
}
