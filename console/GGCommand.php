<?php namespace JZ\GeoguessrReminder\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use JZ\GeoguessrReminder\Controllers\ApiController;
use JZ\GeoguessrReminder\Models\League;
use Keios\SlackNotifications\Classes\SlackMessageSender;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GGCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'gg:cmd';

    protected $signature = 'gg:cmd {cmd?} {identifier?} {param?}';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->fakeCommand();
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

    private function fakeCommand()
    {
        $config = [
            'token' => env('MM_TOKEN'),
            'user_name' => 'jakub',
            'text' => $this->argument('cmd') . ' ' .$this->argument('identifier') . ' '. $this->argument('param'),
        ];
        $c = new ApiController();
        $res = $c->manage($config);
        dd($res);
    }
}
