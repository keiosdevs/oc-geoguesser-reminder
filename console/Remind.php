<?php namespace JZ\GeoguessrReminder\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use JZ\GeoguessrReminder\ApiCommands\Announce;
use JZ\GeoguessrReminder\Controllers\ApiController;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use Keios\SlackNotifications\Classes\SlackMessageSender;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Remind extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'gg:remind';

    protected $signature = 'gg:remind {cmd?} {identifier?} {param?}';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle(): void
    {
        /** @var League[] $leagues */
        $leagues = League::where('is_enabled', true)->where('rounds_left', '>=', 0)->get();
        foreach ($leagues as $league) {
            $this->output->writeln('Processing ' . $league->name);
            $diffHours = $league->getHoursRemaining();
            $this->comment($league->name . ' ' . $diffHours . ' hours left');
            $hoursToRemind = explode(',', $league->hours_to_remind);
            $biggestHour = array_first($hoursToRemind);
            $count = 0;
            if ($diffHours > $biggestHour) {
                $this->comment('Flags timeout, resetting');
                $league->flags = null;
            }
            if ($diffHours === 0) {
                $league->resetNotifications();
            }
            $league->autoblockNotifications();
            foreach ($hoursToRemind as $hourToRemindString) {
                $hourToRemind = (int)$hourToRemindString;
                if ($diffHours === $hourToRemind && !str_contains($league->flags, $hourToRemind)) {
                    if ($count === 0) {
                        $league->flags = $hourToRemind;
                    } else {
                        $league->flags .= ',' . $hourToRemind;
                    }
                    $this->sendReminder($league, $diffHours);
                }
                ++$count;
            }
            $league->save();
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments(): array
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions(): array
    {
        return [];
    }

    private function sendReminder(League $league, int $diff): void
    {
        $this->info('Sending reminder for ' . $league->name);
        $slackSender = new SlackMessageSender();
        $slackSender->send($this->generateMessage($league, $diff));
    }

    private function generateMessage(League $league, int $diff): string
    {
        $hours = 'hours';
        if($diff === 1){
            $hours = 'hour';
        }

        return $diff . ' ' . $hours . ' until the end of [' . $league->name . '](' . $league->url . ') leg! '
               . $league->getPlayersToNotifyString();
    }
}
