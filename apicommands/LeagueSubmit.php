<?php


namespace JZ\GeoguessrReminder\ApiCommands;


use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;

class LeagueSubmit extends BaseCommand
{
    protected $template = 'plugins/jz/geoguessrreminder/templates/league_submit.twig';

    public function handle(CommandPayload $payload): JsonResponse
    {
        $variables = [];
        return GGApiResponse::send($this->getContent($variables));
    }
}
