<?php namespace JZ\GeoguessrReminder\Models;

use Cache;
use Carbon\Carbon;
use DateTimeZone;
use JZ\GeoguessrReminder\Classes\GeoGuessrConnector;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Classes\GGHelpers;
use JZ\GeoguessrReminder\Classes\RequestSender;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Exceptions\InvalidGeoGuesserResponseException;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrMap;
use Keios\SlackNotifications\Classes\SlackMessageSender;
use Log;
use Model;
use October\Rain\Database\Collection;
use October\Rain\Database\Relations\BelongsToMany;
use October\Rain\Database\Relations\HasMany;
use October\Rain\Database\Traits\Validation;
use October\Rain\Exception\ApplicationException;

/**
 * League Model
 * @property int                    $id
 * @property string                 $name
 * @property string                 $slug
 * @property string                 $end_date
 * @property boolean                $is_enabled
 * @property int                    $rounds_left
 * @property int                    $round_time
 * @property string                 $blacklist_users @deprecated
 * @property string                 $url
 * @property string                 $results
 * @property Challenge[]|Collection $challenges
 * @method   HasMany                challenges
 * @property string                 $hours_to_remind
 * @property string                 $description
 * @property string                 $flags
 * @property Player[]|Collection    $players
 * @method BelongsToMany players
 * @property string                 $custom_hook @notImplemented
 */
class League extends Model
{
    use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jz_geoguesserreminder_leagues';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var string[]
     */
    public $hasMany = [
        'challenges' => Challenge::class
    ];
    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'players' => [
            Player::class,
            'table' => 'jz_geoguessrreminder_players_leagues',
            'pivot' => ['position', 'is_notified', 'points', 'score']
        ],
    ];
    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        'name'            => 'required',
        'slug'            => 'required',
        'url'             => 'required',
        'end_date'        => 'required',
        'rounds_left'     => 'required',
        'round_time'      => 'required',
        'hours_to_remind' => 'required'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'end_date'
    ];

    /**
     *
     */
    public function afterSave(): void
    {
        $this->clearCache();
    }

    /**
     *
     */
    public function afterCreate(): void
    {
        $this->clearCache();
        $leagueName = $this->name;
        $slackSender = new SlackMessageSender;
        if ($this->is_enabled) {
            $slackSender->send("
##### :crossed_swords: New league is up: $leagueName ! 
[Click here for details and sign up!](" . $this->url . ')');
        }
    }

    /**
     *
     */
    private function clearCache(): void
    {
        Cache::forget('league_' . $this->id);
        Cache::forget('league_' . $this->slug);
        Cache::forget('leagues_enabled');
        Cache::forget('leagues');
    }

    /**
     * @return string
     */
    public function getPlayersToNotifyString(): string
    {
        $results = '';
        /** @var Player $player */
        foreach ($this->players->where('mm_nick', '!=', null) as $player) {
            if ($player->pivot->is_notified) {
                $results .= $player->mm_nick . ' ';
            }
        }
        return trim($results);
    }

    /**
     * @param $player
     *
     * @todo replace
     */
    public function blacklistPlayer($player): void
    {
        throw new \ApplicationException('treid to bl');
        $player = $this->players->where('mm_nick', $player)->first();
        $player->pivot->is_notified = 0;
        $player->pivot->save();
    }

    /**
     * @param $player
     *
     * @todo replace
     */
    public function whitelistPlayer($player): void
    {
        $player = $this->players->where('mm_nick', $player)->first();
        $player->pivot->is_notified = 1;
        $player->pivot->save();
        $player->save();
    }

    public function getWinner(){
        return $this->players->where('pivot.position', 1)->first();
    }
    public function getWinners(){
        return $this->players->where('pivot.position', '<', 4)->sortBy('pivot.position');
    }

    /**
     * @param string $nick
     *
     * @return bool
     */
    public function isBlacklisted(string $nick): bool
    {
        return $this->players->where('mm_nick', $nick)->where('pivot.is_notified', 0)->count() > 0;
    }

    /**
     * @param string $player
     */
    public function submitPlayer(string $player): void
    {
        if (!str_contains($this->players, $player)) {
            $this->players .= ' ' . $player;
            $this->save();
        }
    }

    /**
     * @return Carbon
     */
    public function getChallengeEndTime(): Carbon
    {
        $endsAt = new Carbon($this->end_date);
        $now = Carbon::now();
//            $now->addHours(1);
        $isPast = $now->diffInSeconds($endsAt, false);
        while ($isPast < 0) {
            $endsAt->addHours($this->round_time);
            $isPast = $now->diffInSeconds($endsAt, false);
        }
        return $endsAt;
    }

    /**
     * @return string
     */
    public function getTimeRemaining(): string
    {
        return GGHelpers::time($this->getSecondsRemaining());
    }

    /**
     * @return int
     */
    public function getHoursRemaining(): int
    {
        $now = Carbon::now();
        $seconds = $this->getChallengeEndTime()->diffInSeconds($now);
        $hours = (int)round($seconds / 60 / 60, 0);

        return $hours;
    }

    /**
     * @return int
     */
    public function getSecondsRemaining(): int
    {
        $now = Carbon::now();
        return $this->getChallengeEndTime()->diffInSeconds($now);
    }

    /**
     * @return string
     */
    public function getLinkMarkdown(): string
    {
        return '[' . $this->name . '](' . $this->url . ')';
    }

    /**
     * @return array|mixed
     */
    public function getRemoteScoreList(): array
    {
        $connector = new GeoGuessrConnector();
        $results = $connector->getRemoteScoreList($this);
        (app()->make(LeagueRepository::class))->synchronize($this, $results);
        return $results;
    }

    /**
     * @return array|mixed
     */
    public function getRemoteDetails(): array
    {
        $connector = new GeoGuessrConnector();
        return $connector->getRemoteDetails($this);
    }

    /**
     * @return string
     */
    public function getCurrentLegId(): string
    {
        $remoteResponse = $this->getRemoteDetails();
        if (!array_key_exists('league', $remoteResponse)) {
            return '';
        }
        $remoteDetails = $remoteResponse['league'];
        if (array_key_exists('currentLeg', $remoteDetails) && $remoteDetails['currentLeg'] !== null) {
            return $remoteDetails['currentLeg']['challengeId'];
        }
        return '';
    }

    /**
     *
     */
    public function setRemainingLegs(): void
    {
        $remoteResponse = $this->getRemoteDetails();
        if (!array_key_exists('league', $remoteResponse)) {
            Log::error('NO LEAGUE IN RESPONSE . ' . PHP_EOL . print_r($remoteResponse, true));
            return;
        }
        $remoteDetails = $remoteResponse['league'];
        $legs = $remoteDetails['totalLegs'];

        if (array_key_exists('currentLeg', $remoteDetails) && $remoteDetails['currentLeg'] !== null) {
            $currentLeg = $remoteDetails['currentLeg']['legNumber'];
            $endTime = new Carbon($remoteDetails['currentLeg']['endsAt'], new DateTimeZone('UTC'));
            $localEndTime = new Carbon($this->end_date, new DateTimeZone('UTC'));

            if ($endTime->timestamp !== $localEndTime->timestamp) {

                $this->end_date = $endTime->setTimezone(new DateTimeZone('Europe/Warsaw'));
                $this->save();
            }
            $remainingLegs = $legs - $currentLeg;
            if ($this->rounds_left != $remainingLegs) {
                $this->rounds_left = $remainingLegs;
                $this->save();
            }
        }
        if (array_key_exists('currentLeg', $remoteDetails) && $remoteDetails['currentLeg'] === null) {
            $this->rounds_left = -1;
            $this->is_enabled = false;
            $this->save();
        }
    }

    /**
     * @return Challenge|null
     */
    public function getCurrentLeg(): ?Challenge
    {
        return $this->challenges->where('is_enabled', true)
                                ->where('ends_at', '>', Carbon::now())
                                ->sortByDesc('created_at')
                                ->first();
    }
    /**
     * @throws InvalidGeoGuesserResponseException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function synchronizeLeg(): void
    {
        $legId = $this->getCurrentLegId();
        if (!$legId) {
            return;
        }

        /** @var ChallengeRepository $challengeRepo */
        $challengeRepo = app()->make(ChallengeRepository::class);
        $details = (new GeoGuessrConnector())->getChallengeDetails($legId);
        // load score list and sync league
        $this->getRemoteScoreList();
        $payload = new CommandPayload([
                                          'user_name' => 'wizard',
                                          'text'      => 'announce ' . $legId . ' silent'
                                      ]);
        $map = new GeoGuessrMap($details, $payload);
        try {
            $challengeDetails = (new GeoGuessrConnector())->getChallengeScores($payload->identifier);
        } catch (InvalidGeoGuesserResponseException $e) {
            // todo permission error handling
            $challengeDetails = [];
        }
        $challenge = new GeoGuessrChallenge($challengeDetails, $this, $map);
        $challengeRepo->deactivateOldLegs($this, $legId);
        $challengeRepo->synchronize($challenge, $this);
    }

    /**
     *
     */
    public function resetNotifications(): void
    {
        foreach ($this->players->where('mm_nick', '!=', null) as $player) {
            $player->pivot->is_notified = 1;
            $player->save();
        }
    }

    /**
     *
     */
    public function autoblockNotifications(): void
    {
        $currentLeg = $this->getCurrentLeg();
        if (!$currentLeg) {
            return;
        }
        foreach ($currentLeg->players->where('mm_nick', '!=', null) as $challengePlayer) {
            if($this->id === 7) {
                throw new ApplicationException('nope no silence');
            }
            $player = $this->players->where('mm_nick', $challengePlayer->mm_nick)->first();
            $player->pivot->is_notified = 0;
            $player->pivot->save();
        }
    }


    /**
     * @return string
     * @deprecated
     */
    public function getPlayers(): string
    {
        $blacklist = explode(' ', $this->blacklist_users);
        $result = $this->players;
        foreach ($blacklist as $player) {
            $result = str_replace($player, '', $result);
        }
        $result = str_replace('  ', ' ', $result);

        return trim($result);
    }

}
