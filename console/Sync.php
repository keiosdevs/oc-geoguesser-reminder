<?php namespace JZ\GeoguessrReminder\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use JZ\GeoguessrReminder\Classes\GeoGuessrConnector;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Controllers\ApiController;
use JZ\GeoguessrReminder\Exceptions\InvalidGeoGuesserResponseException;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrMap;
use Keios\SlackNotifications\Classes\SlackMessageSender;

class Sync extends Command
{
    private const SLEEP_BETWEEN_REQUESTS = 1;
    /**
     * @var string The console command name.
     */
    protected $name = 'gg:sync';

    protected $signature = 'gg:sync {all?}';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     * @throws InvalidGeoGuesserResponseException
     */
    public function handle(): void
    {
        /** @var ChallengeRepository $challengeRepo */
        $challengeRepo = app()->make(ChallengeRepository::class);
        /** @var League[] $leagues */
        $leagues = League::where('is_enabled', true)->where('rounds_left', '>', -1)->get();
        if ($this->argument('all')) {
            $challenges = $challengeRepo->getAll();
        } else {
            $challenges = $challengeRepo->getActive();
        }
        $this->info('Synchronizing leagues...');
        $this->output->progressStart($leagues->count());
        foreach ($leagues as $league) {
            $this->output->writeln('Processing ' . $league->name);
            $league->setRemainingLegs();
            $diffHours = $league->getHoursRemaining();
            $this->comment($league->name . ' ' . $diffHours . ' hours left');
            try {
                // todo remove this - challenges are synced on thier own later
                $league->synchronizeLeg();
            } catch (InvalidGeoGuesserResponseException $e) {
                $this->error('Error when synchronizing current leg: ' . $e->getMessage());
            }
            $hoursToRemind = explode(',', $league->hours_to_remind);
            $biggestHour = array_first($hoursToRemind);

            if ($diffHours > $biggestHour) {
                $this->comment('Flags timeout, resetting');
                $league->flags = null;
            }
            $league->save();
            sleep(self::SLEEP_BETWEEN_REQUESTS);
            $this->output->progressAdvance(1);
        }
        $this->output->progressFinish();
        $this->comment(PHP_EOL);
        $this->info('Synchronizing challenges...');
        $this->output->progressStart($challenges->count());
        foreach ($challenges as $challenge) {
            sleep(self::SLEEP_BETWEEN_REQUESTS);
            $payload = new CommandPayload([
                                              'user_name' => 'wizard',
                                              'text'      => 'announce ' . $challenge->hash . ' silent'
                                          ]);
            $scores = [];
            $details = (new GeoGuessrConnector())->getChallengeDetails($payload->identifier);
            try {
                $scores = (new GeoGuessrConnector())->getChallengeScores($payload->identifier);
            } catch (InvalidGeoGuesserResponseException $e) {
                $this->error('Error when synchronizing current leg: ' . $e->getMessage());
            }

            $map = new GeoGuessrMap($details, $payload);
            $challengeObject = new GeoGuessrChallenge($scores, null, $map);
            $challengeRepo->synchronize($challengeObject);

            $this->output->progressAdvance(1);
        }
        $this->output->progressFinish();
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments(): array
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions(): array
    {
        return [];
    }
}
