<?php


namespace JZ\GeoguessrReminder\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\ApiCommands\Announce;
use JZ\GeoguessrReminder\ApiCommands\ChallengeResults;
use JZ\GeoguessrReminder\ApiCommands\Cup;
use JZ\GeoguessrReminder\ApiCommands\Help;
use JZ\GeoguessrReminder\ApiCommands\LeagueResults;
use JZ\GeoguessrReminder\ApiCommands\RestoreNotifications;
use JZ\GeoguessrReminder\ApiCommands\StopNotifications;
use JZ\GeoguessrReminder\ApiCommands\Time;
use JZ\GeoguessrReminder\ApiCommands\Welcome;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use October\Rain\Exception\ApplicationException;

/**
 * Class ApiController
 * @package JZ\GeoguessrReminder\Controllers
 */
class ApiController
{
    /**
     * @var mixed
     */
    private $token;

    /**
     * @var
     */
    private $leagues;

    /**
     * @var LeagueRepository
     */
    private $leagueRepository;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        $this->token = env('MM_TOKEN', '');
        $this->leagueRepository = app()->make(LeagueRepository::class);
        $this->leagues = $this->leagueRepository->getEnabled();
    }


    /**
     * @return JsonResponse
     * @throws ApplicationException
     */
    public function manage(array $payloadData = []): JsonResponse
    {
        $msg = null;
        $leagueId = null;
        if (empty($payloadData)) {
            $payloadData = get();
        }
        try {
            $this->validateGeoguessrData($payloadData);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500); // todo
        }
        $payload = new CommandPayload($payloadData);
        \Log::info($payload->player . ' has requested following: ' . print_r($payload, true));
        if ($payload->command === 'help') {
            $command = app()->make(Help::class);
            return $command->handle($payload);
        }

        if (!$payload->command) {
            $command = app()->make(Welcome::class);
            return $command->handle($payload);
        }

        if (array_key_exists($payload->command, app()['api_commands'])) {
            $config = app()['api_commands'][$payload->command];
            try {
                /** @var ApiCommand $command */
                $command = app()->make($config['class']);
            } catch (BindingResolutionException $e) {
                return GGApiResponse::send('ERROR. Invalid config for command ' .
                                           $payload->command . ' - please contact support.');
            }
            $command->setConfig($config);
            return $command->handle($payload);
        }


        return GGApiResponse::send('Command not found. See `/gg help` for available commands', 'ephemeral');
    }

    /**
     * @return string
     */
    private function getLeaguesString(): string
    {
        $leaguesList = '';
        foreach ($this->leagues as $league) {
            $leaguesList .= '
- ' . $league->id . '. ' . $league->getLinkMarkdown() . PHP_EOL;
        }
        return $leaguesList;
    }

    /**
     * @param array $data
     *
     * @throws ApplicationException
     */
    private function validateGeoguessrData(array $data): void
    {
        if (!isset($data['user_name'])) {
            throw new ApplicationException('Invalid payload');
        }
        if ($data['token'] !== $this->token) {
            throw new ApplicationException('Invalid token');
        }
    }
}
