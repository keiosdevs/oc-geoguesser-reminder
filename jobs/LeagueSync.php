<?php


namespace JZ\GeoguessrReminder\Jobs;


use JZ\GeoguessrReminder\Classes\GeoGuessrConnector;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Exceptions\InvalidGeoGuesserResponseException;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrMap;
use Keios\Apparatus\Classes\JobManager;
use Keios\Apparatus\Contracts\ApparatusQueueJob;

class LeagueSync implements ApparatusQueueJob
{

    private const SLEEP_BETWEEN_REQUESTS = 1;

    private $jobId;

    public function __construct()
    {

    }

    public function handle(JobManager $jobManager){
        /** @var League[] $leagues */
        $leagues = League::where('is_enabled', true)->where('rounds_left', '>', -1)->get();
        $jobManager->startJob($this->jobId, $leagues->count());
        $count = 0;
        $metadata = [];
        foreach ($leagues as $league) {
            ++$count;
            $metadata = $jobManager->getMetadata($this->jobId);
            if(array_key_exists('failed_legs', $metadata)) {
                $metadata['failed_legs'] = json_decode($metadata['failed_legs'], true);
            }
            $league->setRemainingLegs();
            $diffHours = $league->getHoursRemaining();
            try {
                // todo remove this - challenges are synced on thier own later
                $league->synchronizeLeg();
            } catch (InvalidGeoGuesserResponseException $e) {
                if (!isset($metadata['failed_legs'])) {
                    $metadata['failed_legs'] = [];
                }
                $metadata['failed_legs'][] = ('Error when synchronizing league leg ' . $league->id
                                              . ' '. $e->getMessage());
            }
            $hoursToRemind = explode(',', $league->hours_to_remind);
            $biggestHour = array_first($hoursToRemind);
            if ($diffHours > $biggestHour) {
                $league->flags = null;
            }
            $league->save();
            sleep(self::SLEEP_BETWEEN_REQUESTS);
            if(array_key_exists('failed_legs', $metadata)) {
                $metadata['failed_legs'] = json_encode($metadata['failed_legs']);
            }
            $jobManager->updateMetadata($this->jobId, $metadata);
            $jobManager->updateJobState($this->jobId, $count);
        }
        $jobManager->completeJob($this->jobId, $metadata);

    }


    public function assignJobId(int $id)
    {
        $this->jobId = $id;
    }
}
