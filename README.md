## GeoGuessr Reminder

This plugin allows setting up reminders for your GeoGuessr leagues. Bit more, too.

It's basically a bot for Mattermost or Slack (not tested on Slack though).

![gg1](https://i.viamage.com/jz/stodola-2021-03-13-19-21-39.png)

![gg2](https://i.viamage.com/jz/stodola-2021-03-14-00-10-34.png)

![gg3](https://i.viamage.com/jz/stodola-2021-03-13-19-22-45.png)

## Setup

1. Prepare a working OctoberCMS instance.
2. Install [keios.slacknotifier](https://bitbucket.org/keiosdevs/oc-slacknotifier)
3. Put your token and map nicks in Backend -> GeoGuessr Settings
4. Setup Mattermost/Slack webhook in Backend -> Slack Notifier settings. Add MM_TOKEN to .env
5. Remember to setup `schedule:run` command in your Cron.
6. Add console command (for example /gg) with following GET endpoint: 'https://yourdomain.com/v1/gg'
7. Test it out with `/gg`

- To setup web page, create a page in your CMS and drop "gg_leagues" component into it.


## Usage

When create a league, you will need to put end_time. Put any approximate, it will sync on first cron run.

## TODO

- Renewing GG tokens automatically
- Cleanup PoC mess
- Disabling leagues when rounds = 0;

## Notes

Was written within two evenings, sorry.
