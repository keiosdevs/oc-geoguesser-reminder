<?php


namespace JZ\GeoguessrReminder\ApiCommands;


use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;

/**
 * Class Me
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Me extends BaseCommand
{
    private $playerRepository;

    protected $template = 'plugins/jz/geoguessrreminder/templates/me.twig';

    public function __construct(PlayerRepository $playerRepository)
    {
        $this->playerRepository = $playerRepository;
    }

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        $player = $this->playerRepository->getByMattermostName($payload->player);
        return GGApiResponse::send($this->getContent(['player' => $player]), $payload->type);
    }

    public function getHelp(): string
    {
        return 'shows your stats';
    }
}
