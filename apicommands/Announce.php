<?php


namespace JZ\GeoguessrReminder\ApiCommands;

use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GeoGuessrConnector;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Exceptions\InvalidGeoGuesserResponseException;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrMap;
use Twig;

/**
 * Class Announce
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Announce extends BaseCommand
{
    public const TEMPLATE = 'plugins/jz/geoguessrreminder/templates/announce.twig';

    /**
     * @param CommandPayload    $payload
     *
     * @return JsonResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @var ChallengeRepository $repo
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        return GGApiResponse::send($this->generateMessage($payload), 'in_channel');
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return 'announces GeoGuessr challenge (do not use for leagues)';
    }

    private function generateMessage(CommandPayload $payload): string
    {
        $conn = new GeoGuessrConnector();
        $details = $conn->getChallengeDetails($payload->identifier);
        $map = new GeoGuessrMap($details, $payload);

        $text = \Twig::parse($this->getContent(['map' => $map]));

        return $text;
    }

    /**
     * @param array $variables
     *
     * @return string
     */
    public function getContent(array $variables): string
    {
        $template = file_get_contents(base_path(self::TEMPLATE));
        return Twig::parse($template, $variables);
    }

    /**
     * @param CommandPayload $payload
     *
     * @return string
     */
    private function customType(CommandPayload $payload): string
    {
        $type = 'in_channel';
        if ($payload->param1 === 'silent') {
            $type = 'ephemeral';
        }
        return $type;
    }
}
