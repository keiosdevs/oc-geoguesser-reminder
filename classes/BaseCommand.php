<?php

namespace JZ\GeoguessrReminder\Classes;

use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use Twig;

abstract class BaseCommand implements ApiCommand
{
    protected $config = [];
    protected $template = 'plugins/jz/geoguessrreminder/templates/generic.twig';

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    abstract public function handle(CommandPayload $payload): JsonResponse;

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return '';
    }

    /**
     * @param array $variables
     *
     * @return string
     */
    public function getContent(array $variables): string
    {
        $template = file_get_contents(base_path($this->template));
        return Twig::parse($template, $variables);
    }

    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    public function generateSilentAnnouncePayload($challengeHash): CommandPayload
    {
        return new CommandPayload([
                                      'user_name' => 'wizard',
                                      'text'      => 'announce ' . $challengeHash . ' silent'
                                  ]);
    }
}
