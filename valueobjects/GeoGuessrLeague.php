<?php


namespace JZ\GeoguessrReminder\ValueObjects;


use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\Models\Settings;

class GeoGuessrLeague
{

    public $positions = [];

    /**
     * GeoGuessrChallenge constructor.
     *
     * @param array $data
     */
    public function __construct(array $data, League $league = null)
    {
        /** @var Settings $settings */
        $settings = Settings::instance();
        $map = $settings->getNickMap();
        $nick = $data['playerName'];
        if(array_key_exists($nick, $map)){
            $nick = $map[$nick];
        }
        $result = [];
        foreach ($data as $pos => $details) {
            $result[$pos] = new Position(
                $pos,
                $nick,
                $details['totalPoints'],
                $details['totalScore'],
                $details['pinUrl']
            );
        }
        $this->positions = $result;
    }

    /**
     * @param $seconds
     *
     * @return string
     */
    public function time($seconds)
    {
        $t = round($seconds);
        return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
    }
}
