<?php


namespace JZ\GeoguessrReminder\ApiCommands;

use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Controllers\ApiController;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use Twig;

/**
 * Class Help
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Help extends BaseCommand
{
    protected $template = 'plugins/jz/geoguessrreminder/templates/help.twig';

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        return GGApiResponse::send($this->generateUsage(), $payload->type);
    }

    /**
     * @return string
     */
    private function generateUsage(): string
    {
        $commands = [];
        foreach (app()['api_commands'] as $trigger => $command) {
            $commandPayload = [
                'trigger' => $trigger,
                'class' => app()->make($command['class'])
            ];
            if (isset($command['param'])) {
                $commandPayload['param'] = $command['param'];
            }
            $commands[] = $commandPayload;
        }

        return $this->getContent(['commands' => $commands]);
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return 'shows this.';
    }
}
