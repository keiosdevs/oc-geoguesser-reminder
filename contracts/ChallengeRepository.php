<?php


namespace JZ\GeoguessrReminder\Contracts;


use Illuminate\Pagination\LengthAwarePaginator;
use JZ\GeoguessrReminder\Models\Challenge;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use October\Rain\Database\Collection;

/**
 * Interface ChallengeRepository
 * @package JZ\GeoguessrReminder\Contracts
 */
interface ChallengeRepository
{
    /**
     * @param int $id
     *
     * @return Challenge|null
     */
    public function getById(int $id): ?Challenge;

    /**
     * @param string $hash
     *
     * @return Challenge|null
     */
    public function getByHash(string $hash): ?Challenge;

    /**
     * @return Collection
     */
    public function getActive(): Collection;

    /**
     * @param GeoGuessrChallenge $challenge
     * @param League|null        $league
     *
     * @return Challenge
     */
    public function synchronize(GeoGuessrChallenge $challenge, League $league = null): Challenge;

    /**
     * @return Collection|Challenge[]
     */
    public function getAll(): Collection;
    /**
     * @return LengthAwarePaginator|Challenge[]
     */
    public function getAllPaginated(): LengthAwarePaginator;

    public function deactivateOldLegs(League $league, string $currentLegHash);
}
