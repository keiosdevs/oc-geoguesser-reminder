<?php


namespace JZ\GeoguessrReminder\Repositories;

use Illuminate\Pagination\LengthAwarePaginator;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\Models\League;
use October\Rain\Database\Collection;

class EloquentLeagueRepository implements LeagueRepository
{
    private $playerRepository;

    public function __construct(PlayerRepository $playerRepository)
    {
        $this->playerRepository = $playerRepository;
    }

    public function getById(int $leagueId): ?League
    {
        return League::where('id', $leagueId)
                     ->with('players')
                     ->with('challenges')
                     ->rememberForever('league_' . $leagueId)->first();
    }

    public function getBySlug(string $slug): ?League
    {
        return League::where('slug', $slug)
            ->with('players')
            ->with('challenges')
            ->rememberForever('league_' . $slug)
            ->first();
    }

    public function getEnabled(): Collection
    {
        return League::where('is_enabled', true)
            ->with('players')
            ->with('challenges')
            ->orderBy('created_at', 'desc')
            ->rememberForever('leagues_enabled')
            ->get();
    }

    public function getAll(): Collection
    {
        return League::orderBy('created_at', 'desc')
            ->with('players')
            ->with('challenges')
            ->rememberForever('leagues')
            ->get();
    }

    public function getAllPaginated(int $perPage = 10): LengthAwarePaginator
    {
        return League::with('challenges')->orderBy('is_enabled', 'desc')->orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function synchronize(League $league, array $scoreList): League
    {
        foreach ($scoreList as $pos => $score) {
            $position = $pos + 1;
            if (!isset($score['user'])) {
                continue;
            }
            $player = $this->playerRepository->getByGeoguessrName($score['user']['nick']);
            if (!$player) {
                continue;
            }

            if ($attachedLeague = $player->leagues->where('id', $league->id)->first()) {
                // todo read notification config
                $player->leagues()->remove($attachedLeague);
                $player->save();
                $player->leagues()->add(
                    $league,
                    null,
                    [
                        'position' => $position,
                        'points'   => $score['totalPoints'],
                        'score'    => $score['totalScore']
                    ]
                );
            } else {
                $player->leagues()->add($league, null, [
                    'position' => $position,
                    'points'   => $score['totalPoints'],
                    'score'    => $score['totalScore']
                ]);
            }
            $player->save();
        }
        $league->results = json_encode($scoreList);
        $league->save();

        return $league;
    }
}
