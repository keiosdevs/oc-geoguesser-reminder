<?php


namespace JZ\GeoguessrReminder\Classes;

use JZ\GeoguessrReminder\Models\Settings;

/**
 * Class GGHelpers
 * @package JZ\GeoguessrReminder\Classes
 */
class GGHelpers
{
    // todo move to config or sth
    public const GOLD_VALUE = 3;
    public const SILVER_VALUE = 2;
    public const BRONZE_VALUE = 1;
    public const BRONZE_LEAGUE_VALUE = 5;
    public const SILVER_LEAGUE_VALUE = 10;
    public const GOLD_LEAGUE_VALUE = 15;
    /**
     * @param $seconds
     *
     * @return string
     */
    public static function timeText($seconds): string
    {
        $t = round($seconds);
        $hours = (int)($t / 3600);
        $minutes = (int)($t / 60 % 60);
        $days = 0;
        if ($hours > 24) {
            $days = (int)($hours / 24);
        }
        $text = '';
        if ($days) {
            if ($days === 1) {
                $text .= $days . ' day ';
            } else {
                $text .= $days . ' days ';
            }
        } else {
            $text .= $hours . ' hours ' . $minutes . ' minutes';
        }
        return $text;
    }

    /**
     * @param int $seconds
     *
     * @return string
     */
    public static function time(int $seconds): string
    {
        $t = round($seconds);
        return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
    }

    /**
     * @param string $mmNick
     *
     * @return string|null
     */
    public static function findGeoguessrNickOnMap(string $mmNick): ?string
    {
        $nickMap = Settings::instance()->getNickMap();
        $map = array_flip($nickMap);
        return $map[$mmNick] ?? null;
    }

    /**
     * @param string $baseNick
     *
     * @return string|null
     */
    public static function findMmNickOnMap(string $baseNick): ?string
    {
        $nickMap = Settings::instance()->getNickMap();
        if (array_key_exists($baseNick, $nickMap)) {
            return '@'.$nickMap[$baseNick];
        }
        return null;
    }
}
