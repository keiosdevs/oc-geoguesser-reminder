<?php


namespace JZ\GeoguessrReminder\ValueObjects;

/**
 * Class GeoGuessrData
 * @package JZ\GeoguessrReminder\ValueObjects
 */
class GeoGuessrData
{
    /**
     * @var GeoGuessrMap
     */
    public $map;
    /**
     * @var GeoGuessrChallenge
     */
    public $challenge;
    /**
     * @var GeoGuessrLeague
     */
    public $league;

    /**
     * GeoGuessrData constructor.
     */
    public function __construct(
        GeoGuessrMap $map = null,
        GeoGuessrChallenge $challenge = null,
        GeoGuessrLeague $league = null
    ) {
        if ($map) {
            $this->map = $map;
        }
        if ($challenge) {
            $this->challenge = $challenge;
        }
        if ($league) {
            $this->league = $league;
        }
    }
}
