<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddLeagueTable extends Migration
{
    public function up()
    {
        Schema::table('jz_geoguesserreminder_leagues', function (Blueprint $table) {
            $table->longText('results')->nullable()->after('description');
        });
    }

    public function down()
    {
        Schema::table('jz_geoguesserreminder_leagues', function (Blueprint $table) {
            $table->dropColumn('results');
        });
    }
}
