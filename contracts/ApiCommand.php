<?php


namespace JZ\GeoguessrReminder\Contracts;


use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;

interface ApiCommand
{
    public function handle(CommandPayload $payload): JsonResponse;

    public function getHelp(): string;

    public function getContent(array $params): string;

    public function setConfig(array $config): void;
}
