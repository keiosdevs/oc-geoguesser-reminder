<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddLeaguePivotFields extends Migration
{
    public function up()
    {
        Schema::table('jz_geoguessrreminder_players_leagues', function (Blueprint $table) {
            $table->integer('points')->nullable()->after('position');
            $table->integer('score')->nullable()->after('points');
        });

    }

    public function down()
    {
        Schema::table('jz_geoguessrreminder_players_leagues', function (Blueprint $table) {
            $table->dropColumn('score');
            $table->dropColumn('points');
        });
    }
}
