<?php


namespace JZ\GeoguessrReminder\ApiCommands;


use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\Jobs\ChallengesSync;
use JZ\GeoguessrReminder\Jobs\LeagueSync;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use Keios\Apparatus\Classes\JobManager;

/**
 * Class Me
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Resync extends BaseCommand
{
    protected $template = 'plugins/jz/geoguessrreminder/templates/resync.twig';

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        $all = false;
        if($payload->identifier === 'all'){
            $all = true;
        }
        /** @var JobManager $jobManager */
        $jobManager = app()->make(JobManager::class);
        $challengeJob = new ChallengesSync($payload, $all);
        $leaguesJob = new LeagueSync();
        $jobManager->dispatch($leaguesJob, 'gg league sync triggered by '. $payload->player);
        $jobManager->dispatch($challengeJob, 'gg challenge sync triggered by '. $payload->player);

        return GGApiResponse::send('Tasks started! Wait for it...', 'ephemeral');
    }

    public function getHelp(): string
    {
        return '';
    }
}
