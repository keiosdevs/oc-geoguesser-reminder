<?php


namespace JZ\GeoguessrReminder\Models;

use October\Rain\Database\Pivot;

/**
 * User-Role Pivot Model
 */
class PlayerChallengePivot extends Pivot
{

    protected $table = 'jz_geoguessrreminder_players_legs';

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Rules
     */
    public $rules = [    ];

}
