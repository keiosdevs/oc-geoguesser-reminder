<?php


namespace JZ\GeoguessrReminder\ApiCommands;

use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GeoGuessrConnector;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Exceptions\InvalidGeoGuesserResponseException;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\Models\Settings;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrMap;
use JZ\GeoguessrReminder\ValueObjects\Position;
use Twig;

/**
 * Class LeagueResults
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class LeagueResults extends BaseCommand
{
    private $leagueTemplate = 'plugins/jz/geoguessrreminder/templates/league_results.twig';
    private $challengeTemplate = 'plugins/jz/geoguessrreminder/templates/leg_results.twig';

    private $challengeRepository;
    private $leagueRepository;

    public function __construct(ChallengeRepository $challengeRepository, LeagueRepository $leagueRepository)
    {
        $this->challengeRepository = $challengeRepository;
        $this->leagueRepository = $leagueRepository;
    }

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        if (is_numeric($payload->identifier)) {
            $league = $this->leagueRepository->getById($payload->identifier);
        } else {
            $league = $this->leagueRepository->getBySlug($payload->identifier);
        }
        if (!$league) {
            return GGApiResponse::send('League not found.', 'ephemeral');
        }
        $challengeContent = '';
        $leagueContent = $this->generateTable($league, $payload->player);
        $currentLegId = $league->getCurrentLegId();
        if ($currentLegId) {
            $challengePayload = new CommandPayload([
                                               'user_name' => $payload->player,
                                               'text'      => 'legResults ' . $currentLegId
                                           ]);

            $challengeContent = $this->generateChallengeTable($challengePayload, $currentLegId);
        }
        $content = $challengeContent . "\n" . $leagueContent;

        return GGApiResponse::send($content, $payload->type);
    }

    /**
     * @param League $league
     * @param string $playerName
     *
     * @return string
     */
    private function generateTable(League $league, string $playerName): string
    {
        /** @noinspection UnusedFunctionResultInspection - we only want sync here */
        $league->getRemoteScoreList();
        $variables = [
            'league'    => $league,
            'user'      => $playerName
        ];
        $this->template = $this->leagueTemplate;
        return $this->getContent($variables);
    }

    private function generateChallengeTable(CommandPayload $payload, string $legId): string
    {
        try {
            $challengeDetails = (new GeoGuessrConnector())->getChallengeScores($legId);
        } catch (InvalidGeoGuesserResponseException $e) {
            return 'Error: ' . $e->getMessage() . " (probably jakub didn't play yet)";
        }
        $challengeDetails[0]['hash'] = $legId;
        $map = new GeoGuessrMap($challengeDetails, $payload);
        $challengeObject = new GeoGuessrChallenge($challengeDetails, $payload->league, $map);
        $this->challengeRepository->synchronize($challengeObject);
        $variables = [
            'challenge' => $challengeObject,
            'league'    => $payload->league
        ];

        $this->template = $this->challengeTemplate;
        return $this->getContent($variables);
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return 'shows main table and current leg results for selected league.';
    }
}
