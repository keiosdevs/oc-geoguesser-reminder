<?php namespace JZ\GeoguessrReminder\Models;

use October\Rain\Database\Model;

/**
 * Settings Model
 */
class Settings extends Model
{

    public $jsonable = ['nick_map'];
    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * A unique code of settings
     */
    public $settingsCode = 'jz_gg_settings';

    /**
     * Reference to field configuration
     */
    public $settingsFields = 'fields.yaml';

    public function getNickMap(): array
    {
        $map = $this->nickMap;
        $result = [];
        foreach ($map as $field) {
            $result[$field['gg_nick']] = '@'.$field['mm_nick'];
        }

        return $result;
    }
}
