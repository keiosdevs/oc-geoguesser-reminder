<?php namespace JZ\GeoguessrReminder\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use JZ\GeoguessrReminder\Classes\GGHelpers;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\Controllers\ApiController;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\Models\Player;
use JZ\GeoguessrReminder\Models\Settings;
use Keios\SlackNotifications\Classes\SlackMessageSender;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class ScanPlayers
 * @package JZ\GeoguessrReminder\Console
 */
class ScanPlayers extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'gg:scan-players';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * @var Settings
     */
    protected $settings;

    /**
     * @var array
     */
    protected $nickMap;

    /**
     * @var ChallengeRepository
     */
    protected $challengeRepository;

    /**
     * @var PlayerRepository
     */
    protected $playersRepository;

    /**
     * @var LeagueRepository|mixed
     */
    protected $leagueRepository;

    public function __construct()
    {
        parent::__construct();
        $this->settings = Settings::instance();
        $this->nickMap = $this->settings->getNickMap();
        $this->challengeRepository = app()->make(ChallengeRepository::class);
        $this->playersRepository = app()->make(PlayerRepository::class);
        $this->leagueRepository = app()->make(LeagueRepository::class);
    }

    /**
     * Execute the console command.
     * @return void
     */
    public function handle(): void
    {
        $challenges = $this->challengeRepository->getAll();
        $leagues = $this->leagueRepository->getAll();
        $nickList = [];
        $playersLeagues = [];
        $avatars = [];
        $created = 0;
        $updated = 0;
        foreach ($challenges as $challenge) {
            $positions = json_decode($challenge->positions, true);
            if (is_array($positions)) {
                foreach ($positions as $position) {
                    $baseNick = $position['player'];
                    if (str_contains($baseNick, '@')) {
                        $ggNick = GGHelpers::findGeoguessrNickOnMap($baseNick);
                        $nickList[$ggNick] = $baseNick;
                    } else {
                        $nickList[$baseNick] = GGHelpers::findMmNickOnMap($baseNick);
                        $ggNick = $baseNick;
                    }
                    if ($challenge->league) {
                        if (!array_key_exists($ggNick, $playersLeagues)) {
                            $playersLeagues[$ggNick] = [];
                        }
                        $playersLeagues[$ggNick][$challenge->league_id][$challenge->id] = $challenge->id;
                        if (isset($position['image'])) {
                            $avatars[$ggNick] = $position['image'];
                        }
                    }
                }
            }
            //     dump($nickList);
        }

        foreach ($nickList as $ggName => $mmName) {
            $player = $this->playersRepository->getByGeoguessrName($ggName);
            if (!$player) {
                $player = new Player();
                ++$created;
            }
            $player->gg_nick = $ggName;
            if ($player->mm_nick !== $mmName) {
                ++$updated;
            }
            if ($mmName) {
                $player->mm_nick = $mmName;
            }
            if (isset($avatars[$player->gg_nick]) && $avatars[$player->gg_nick] !== '') {
                $player->avatar = 'https://www.geoguessr.com/images/auto/48/48/ce/0/plain/'
                                  . $avatars[$player->gg_nick];
            }
            $player->save();

            $playerLeagues = $playersLeagues[$player->gg_nick];
            foreach ($playerLeagues as $leagueId => $playerChallenges) {
                if (!in_array($leagueId, $player->leagues->pluck('id', 'id')->toArray(), true)) {
                    $player->leagues()->add($leagues->where('id', $leagueId)->first());
                }
                foreach ($playerChallenges as $challenge) {
                    if (!in_array($challenge, $player->challenges->pluck('id', 'id')->toArray(), true)) {
                        $player->challenges()->add($challenges->where('id', $challenge)->first());
                    }
                }
            }
        }
        $this->info('Created ' . $created, ' Updated '. $updated);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
