<?php namespace JZ\GeoguessrReminder\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use October\Rain\Exception\ApplicationException;

/**
 * Class PlayerComponent
 * @package JZ\GeoguessrReminder\Components
 */
class PlayerComponent extends ComponentBase
{
    /**
     * @var PlayerRepository
     */
    private $playersRepo;

    /**
     * PlayerComponent constructor.
     *
     * @param CodeBase|null $cmsObject
     * @param array         $properties
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(CodeBase $cmsObject = null, $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->playersRepo = app()->make(PlayerRepository::class);
    }

    /**
     * @return string[]
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'Player Component',
            'description' => 'Converts token to player'
        ];
    }

    /**
     * @return array
     */
    public function defineProperties(): array
    {
        return [];
    }

    /**
     *
     */
    public function onRun(): void
    {
        $input = \Input::all();
        $token = $input['token'] ?? null;
        if(!$token){
            $token = \Session::get('token');
        }
        if ($token) {
            $this->page['player'] = $this->playersRepo->getByToken($token);
        }
    }

    public function onValidateToken()
    {
        $token = post('token');
        if(!$token){
            throw new \ApplicationException('No token provided');
        }
        if(!$this->playersRepo->getByToken($token)){
            throw new ApplicationException('Invalid token');
        }
        \Session::put('token', $token);
        return redirect()->to('/gg');
    }

    public function onLogoutPlayer(){
        \Session::forget('token');
        return redirect()->to('/gg');
    }
}
