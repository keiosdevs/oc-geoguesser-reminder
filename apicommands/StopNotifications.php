<?php


namespace JZ\GeoguessrReminder\ApiCommands;


use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Contracts\LeagueRepository;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use OpenCloud\Common\Base;

/**
 * Class StopNotifications
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class StopNotifications extends BaseCommand
{
    private $challengeRepository;
    private $leagueRepository;

    public function __construct(ChallengeRepository $challengeRepository, LeagueRepository $leagueRepository)
    {
        $this->challengeRepository = $challengeRepository;
        $this->leagueRepository = $leagueRepository;
    }

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        if (is_numeric($payload->identifier)) {
            $league = $this->leagueRepository->getById($payload->identifier);
        } else {
            $league = $this->leagueRepository->getBySlug($payload->identifier);
        }
        if (!$league) {
            return GGApiResponse::send('League not found.', 'ephemeral');
        }
        $player = $payload->player;

        if (!$league->isBlacklisted($player)) {
            $league->blacklistPlayer($player);
            return GGApiResponse::send('Notifications for current challenge on league ' .
                                       $league->getLinkMarkdown() . ' have been disabled, ' . $player);
        }

        return GGApiResponse::send('Notifications for current challenge on league ' .
                                   $league->getLinkMarkdown() . ' already disabled, ' . $player);
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return 'stops notifications for selected league.';
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function getContent(array $params): string
    {
        return ''; // todo create twig template
    }
}
