<?php namespace JZ\GeoguessrReminder\Models;

use Carbon\Carbon;
use JZ\GeoguessrReminder\Repositories\EloquentChallengeRepository;
use Model;
use JZ\GeoguessrReminder\Classes\GGHelpers;
use October\Rain\Database\Collection;
use October\Rain\Database\Relations\BelongsToMany;

/**
 * Challenge Model
 * @property int                 $id
 * @property string              $mapName
 * @property bool                $noMove
 * @property bool                $noZoom
 * @property bool                $noRotate
 * @property int                 $time
 * @property int                 $rounds
 * @property string              $hash
 * @property string              $positions
 * @property string|null         $description
 * @property Carbon              $ends_at
 * @property int                 $league_id
 * @property string              $url
 * @property Player[]|Collection $players
 * @method   BelongsToMany       players
 * @property League|null         $league
 * @property                     $is_enabled
 */
class Challenge extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jz_geoguesserreminder_challenges';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var string[]
     */
    public $belongsTo = [
        'league' => League::class
    ];
    /**
     * @var array[]
     */
    public $belongsToMany = [
        'players' => [
            Player::class,
            'table' => 'jz_geoguessrreminder_players_legs',
            'pivot' => ['position', 'points']
        ]
    ];

    public function getEndsAt(){
        return new Carbon($this->ends_at);
    }

    public function beforeSave(): void
    {
        if (!$this->ends_at) {
            $this->ends_at = null;
        }
    }

    public function afterCreate(): void
    {
        $this->cacheClear();
    }

    public function afterSave(): void
    {
        $this->cacheClear();
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        $t = round($this->time);
        return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
    }


    /**
     * @param string $player
     *
     * @return bool
     */
    public function played(string $player): bool
    {
        return str_contains($this->positions, $player);
    }

    /**
     * @return string
     */
    public function getTimeToEnd(): string
    {
        if ($this->ends_at) {
            $now = Carbon::now();
            return GGHelpers::timeText((new Carbon($this->ends_at))->diffInSeconds($now));
        }
    }

    /**
     * @param string $player
     *
     * @return bool
     * @noinspection PhpUnused Twig Method
     */
    public function notSynced(string $player): bool
    {
        if ($this->positions === '[]' && $player !== env('GG_TOKEN_OWNER', '@jakub')) {
            return true;
        }
        return false;
    }

    public function getLeader(){
        return $this->players->sortBy('pivot.position')->sortByDesc('score')->first();
    }

    public function getPosition(int $playerId){
        return $this->players->where('id', $playerId)->sortByDesc('score')->first();

    }

    /**
     * @return string
     * @noinspection PhpUnused Twig Method
     */
    public function getFeaturesString(): string
    {
        $param = '';
        if ($this->noMove) {
            $param .= 'No Move ';
        }
        if ($this->noZoom) {
            $param .= 'No Zoom ';
        }
        if ($this->noRotate) {
            $param .= 'No Rotate';
        }

        return trim($param);
    }

    private function cacheClear(): void
    {
        \Cache::forget(EloquentChallengeRepository::cacheKey.$this->id);
        \Cache::forget(EloquentChallengeRepository::cacheKey.$this->hash);
        \Cache::forget(EloquentChallengeRepository::cacheKey.'_active');
    }
}
