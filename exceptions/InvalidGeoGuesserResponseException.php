<?php


namespace JZ\GeoguessrReminder\Exceptions;

use October\Rain\Exception\ApplicationException;

class InvalidGeoGuesserResponseException extends ApplicationException
{

}
