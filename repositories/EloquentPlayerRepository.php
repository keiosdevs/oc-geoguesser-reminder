<?php


namespace JZ\GeoguessrReminder\Repositories;

use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\Models\Player;
use October\Rain\Database\Collection;

class EloquentPlayerRepository implements PlayerRepository
{

    public function getByGeoguessrName(string $name): ?Player
    {
        return Player::where('gg_nick', $name)->first();
    }

    public function getMattermostPlayers(): Collection
    {
        return Player::where('mm_nick', '!=', null)->get();
    }

    public function createPlayer(string $ggNick): Player
    {
        $player = new Player();
        $player->gg_nick = $ggNick;
        $player->save();

        return $player;
    }

    public function getByMattermostName(string $player): ?Player
    {
        return Player::where('mm_nick', $player)->with('leagues')->with('challenges')->first();
    }

    public function getByToken(string $token): ?Player
    {
        return Player::where('token', $token)->with('leagues')->with('challenges')->first();
    }
}
