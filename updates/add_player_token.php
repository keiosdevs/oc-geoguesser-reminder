<?php namespace JZ\GeoguessrReminder\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddPlayerToken extends Migration
{
    public function up()
    {
        Schema::table('jz_geoguessrreminder_players', function (Blueprint $table) {
            $table->string('token', 255)->nullable()->after('avatar');
        });
    }

    public function down()
    {
        Schema::table('jz_geoguessrreminder_players', function (Blueprint $table) {
            $table->dropColumn('token');
        });
    }
}
