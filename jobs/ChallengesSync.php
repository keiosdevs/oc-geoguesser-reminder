<?php


namespace JZ\GeoguessrReminder\Jobs;

use JZ\GeoguessrReminder\Classes\GeoGuessrConnector;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\Exceptions\InvalidGeoGuesserResponseException;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrChallenge;
use JZ\GeoguessrReminder\ValueObjects\GeoGuessrMap;
use Keios\Apparatus\Classes\JobManager;
use Keios\Apparatus\Contracts\ApparatusQueueJob;
use Keios\SlackNotifications\Classes\SlackMessageSender;

/**
 * Class ChallengesSync
 * @package JZ\GeoguessrReminder\Jobs
 */
class ChallengesSync implements ApparatusQueueJob
{
    /**
     *
     */
    private const SLEEP_BETWEEN_REQUESTS = 1;

    /**
     * @var
     */
    private $jobId;

    /**
     * @var false|mixed
     */
    private $syncAll;

    /**
     * @var CommandPayload
     */
    private $payload;


    /**
     * ChallengesSync constructor.
     *
     * @param CommandPayload $payload
     * @param false          $syncAll
     */
    public function __construct(CommandPayload $payload, $syncAll = false)
    {
        $this->syncAll = $syncAll;
        $this->payload = $payload;
    }

    /**
     * @param JobManager $jobManager
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function handle(JobManager $jobManager)
    {
        /** @var ChallengeRepository $challengeRepo */
        $challengeRepo = app()->make(ChallengeRepository::class);
        /** @var League[] $leagues */

        if ($this->syncAll) {
            $challenges = $challengeRepo->getAll();
        } else {
            $challenges = $challengeRepo->getActive();
        }
        $jobManager->startJob($this->jobId, $challenges->count());
        $count = 1;
        foreach ($challenges as $challenge) {
            ++$count;
            $metadata = $jobManager->getMetadata($this->jobId);
            if(array_key_exists('failed_legs', $metadata)) {
                $metadata['failed_legs'] = json_decode($metadata['failed_legs'], true);
            }
            sleep(self::SLEEP_BETWEEN_REQUESTS);
            $payload = new CommandPayload([
                                              'user_name' => 'wizard',
                                              'text'      => 'announce ' . $challenge->hash . ' silent'
                                          ]);
            $scores = [];
            try {
                $details = (new GeoGuessrConnector())->getChallengeDetails($payload->identifier);
                $scores = (new GeoGuessrConnector())->getChallengeScores($payload->identifier);
            } catch (InvalidGeoGuesserResponseException $e) {
                if (!isset($metadata['failed_legs']) || !is_array($metadata['failed_legs'])) {
                    $metadata['failed_legs'] = [];
                }
                $metadata['failed_legs'][] = ('Error when synchronizing leg ' . $challenge->id . ' '. $e->getMessage());
            }

            $map = new GeoGuessrMap($details, $payload);
            $challengeObject = new GeoGuessrChallenge($scores, null, $map);
            if(array_key_exists('failed_legs', $metadata)) {
                $metadata['failed_legs'] = json_encode($metadata['failed_legs']);
            }
            $challengeRepo->synchronize($challengeObject);
            $jobManager->updateMetadata($this->jobId, $metadata);
            $jobManager->updateJobState($this->jobId, $count);
        }
$notifier = new SlackMessageSender();
        $notifier->send('Manual sync triggered by ' . $this->payload->player . ' finished! :blobdance:');
        $jobManager->completeJob($this->jobId);
    }


    /**
     * @param int $id
     */
    public function assignJobId(int $id)
    {
        $this->jobId = $id;
    }
}
