<?php

namespace JZ\GeoguessrReminder\ApiCommands;

use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Contracts\ChallengeRepository;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use Twig;

/**
 * Class Challenges
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Challenges extends BaseCommand
{
    protected $template = 'plugins/jz/geoguessrreminder/templates/challenges.twig';

    /**
     * @var ChallengeRepository
     */
    private $challengeRepository;

    /**
     * Challenges constructor.
     *
     * @param ChallengeRepository $repository
     */
    public function __construct(ChallengeRepository $repository)
    {
        $this->challengeRepository = $repository;
    }

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        $challenges = $this->challengeRepository->getActive();
        $params = [
            'challenges' => $challenges,
            'player' => $payload->player
        ];


        return GGApiResponse::send($this->getContent($params), $payload->type);
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return 'shows historic challenges';
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function getContent(array $params): string
    {

        $template = file_get_contents(base_path($this->template));
        return Twig::parse($template, $params);
    }
}
