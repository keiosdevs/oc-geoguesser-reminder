<?php


namespace JZ\GeoguessrReminder\ApiCommands;

use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Contracts\PlayerRepository;
use JZ\GeoguessrReminder\Controllers\ApiController;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;
use Twig;

/**
 * Class Help
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Info extends BaseCommand
{
    private $playerRepository;

    protected $template = 'plugins/jz/geoguessrreminder/templates/me.twig';

    public function __construct(PlayerRepository $playerRepository)
    {
        $this->playerRepository = $playerRepository;
    }

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        $player = $this->playerRepository->getByMattermostName($payload->identifier);
        if (!$player) {
            return GGApiResponse::send('Player ' . $payload->identifier . ' not found.');
        }
        return GGApiResponse::send($this->getContent(['player' => $player]), $payload->type);
    }

    public function getHelp(): string
    {
        return 'shows other player stats';
    }
}
