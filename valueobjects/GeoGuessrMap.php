<?php


namespace JZ\GeoguessrReminder\ValueObjects;

/**
 * Class GeoGuessrChallenge
 * @package JZ\GeoguessrReminder\ValueObjects
 */
class GeoGuessrMap
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $code;
    /**
     * @var int
     */
    public $rounds;
    /**
     * @var string
     */
    public $timeLimit;
    /**
     * @var string
     */
    public $params;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $image;

    public $player;

    public $noMove;
    public $noZoom;
    public $noRotate;
    public $time;
    /**
     * GeoGuessrChallenge constructor.
     *
     * @param array $data
     */
    public function __construct(array $details, CommandPayload $payload)
    {
        $data = [];
        if(array_key_exists('challenge', $details)){
            $data = $details['challenge'];
        }
        if(array_key_exists(0, $details) && array_key_exists('game', $details[0])){
            $data = $details[0]['game'];
        }
        if($data){
            $this->noMove = $data["forbidMoving"];
            $this->noZoom = $data["forbidZooming"];
            $this->noRotate = $data["forbidRotating"];
            $this->time = $data['timeLimit'];
            $this->rounds = $data['roundCount'];
        }
        $this->url = 'https://geoguessr.com/challenge/' . $payload->identifier;
        $this->code = $payload->identifier;
        if (array_key_exists('map', $details)) {
            $map = $details['map'];
            $this->name = $map['name'];
        } elseif(array_key_exists('mapName', $data)) {
            $this->name = $data['mapName'];
        }
            $this->player = $payload->player;
            $this->timeLimit = $this->time($data['timeLimit']);
            $this->rounds = $data['roundCount'];
            $this->params = '';
            if (isset($map['images']) && isset($map['images']['backgroundLarge'])) {
                $this->image = "https://www.geoguessr.com/images/auto/288/144/ce/0/plain/" .
                               $map['images']['backgroundLarge'];
            }

            if ($data['forbidMoving']) {
                $this->params .= 'No Move ';
            }
            if ($data['forbidZooming']) {
                $this->params .= 'No Zoom ';
            }
            if ($data['forbidRotating']) {
                $this->params .= 'No Rotate';
            }

    }

    /**
     * @param $seconds
     *
     * @return string
     */
    public function time($seconds)
    {
        $t = round($seconds);
        return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
    }
}
