<?php


namespace JZ\GeoguessrReminder\ApiCommands;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use JZ\GeoguessrReminder\Classes\BaseCommand;
use JZ\GeoguessrReminder\Classes\GGApiResponse;
use JZ\GeoguessrReminder\Classes\GGHelpers;
use JZ\GeoguessrReminder\Contracts\ApiCommand;
use JZ\GeoguessrReminder\Models\League;
use JZ\GeoguessrReminder\ValueObjects\CommandPayload;

/**
 * Class Time
 * @package JZ\GeoguessrReminder\ApiCommands
 */
class Time extends BaseCommand
{

    /**
     * @param CommandPayload $payload
     *
     * @return JsonResponse
     */
    public function handle(CommandPayload $payload): JsonResponse
    {
        $league = $payload->league;
        $diff = GGHelpers::time($league->getSecondsRemaining());

        return GGApiResponse::send('Time left for ' . $league->getLinkMarkdown() .
                                   ': ' . $diff, $payload->type);
    }

    /**
     * @return string
     */
    public function getHelp(): string
    {
        return '';
        //return 'shows remaining time for current leg. obsolete with new /gg';
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function getContent(array $params): string
    {
        return ''; // todo create twig template
    }
}
